* Uses shadow-cljs: https://github.com/thheller/shadow-cljs
* Run the web app using ```npx shadow-cljs watch frontend```
* Put your API key into a file called secrets.edn with the format ```{:api-key "<key-goes-here>"}```
