(ns factools.data.ingest
  (:require [cljs.reader :as reader]))

(def fs (js/require "fs"))

(defn read-edn [path f]
  (.readFile fs path "utf8" (fn [err data] (f (reader/read-string data)))))

(defn process [coll])

(read-edn "secrets.edn" process)

(+ 1 1)
